/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.entity;

/**
 *
 * @author krissadewo
 */
public class Buku {

    private int id; //auto increment by db engine
    private String kode;
    private String judul;
    private String pengarang;

    public Buku() {
    }

    public Buku(String kode, String judul, String pengarang) {
        this.kode = kode;
        this.judul = judul;
        this.pengarang = pengarang;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPengarang() {
        return pengarang;
    }

    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    @Override
    public String toString() {
        return "Buku{" + "id=" + id + ", kode=" + kode + ", judul=" + judul + ", pengarang=" + pengarang + '}';
    }

}
