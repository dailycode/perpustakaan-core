/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import id.co.iconpln.perpustakaan.entity.Buku;

/**
 *
 * @author krissadewo
 */
public interface BukuDAO extends BaseDAO<Buku>{
    
    /**
     * 
     * @param param mean as code book
     * @return 
     */
    int findIdByKode(String param);
     
}
