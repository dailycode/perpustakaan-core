/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.impl;

import id.co.iconpln.perpustakaan.dao.TeleponDAO;
import id.co.iconpln.perpustakaan.entity.Telepon;
import id.co.iconpln.perpustakaan.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TeleponDAOImpl implements TeleponDAO {

    private final Connection connection = DBConnection.getInstance().getConnection();

    @Override
    public Telepon save(Telepon param) {
        String sql = "INSERT INTO telepon("
                + "no,"
                + "id_anggota) "
                + "VALUES(?,?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getNo());
            preparedStatement.setInt(2, param.getAnggota().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BukuDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return param;
    }

    @Override
    public Telepon update(Telepon param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Telepon delete(Telepon param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Telepon findById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Telepon> find(Telepon param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
