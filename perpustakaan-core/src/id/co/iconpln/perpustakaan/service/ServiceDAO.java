/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.service;

import id.co.iconpln.perpustakaan.dao.AnggotaDAO;
import id.co.iconpln.perpustakaan.dao.BukuDAO;
import id.co.iconpln.perpustakaan.dao.TeleponDAO;
import id.co.iconpln.perpustakaan.dao.impl.AnggotaDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.TeleponDAOImpl;
import id.co.iconpln.perpustakaan.entity.Anggota;
import id.co.iconpln.perpustakaan.entity.Buku;
import id.co.iconpln.perpustakaan.entity.Telepon;
import java.util.List;

/**
 *
 * @author krissadewo
 */
public class ServiceDAO {

    private final BukuDAO bukuDAO = new BukuDAOImpl();
    private final AnggotaDAO anggotaDAO = new AnggotaDAOImpl();
    private final TeleponDAO teleponDAO = new TeleponDAOImpl();

    public List<Buku> findBukus(Buku param) {
        return bukuDAO.find(param);
    }

    public Buku save(Buku param) {
        return bukuDAO.save(param);
    }

    public Anggota save(Anggota param) {
        Anggota result = anggotaDAO.save(param);
        for(Telepon telepon : param.getTelepons()){
            telepon.setAnggota(result);
            teleponDAO.save(telepon);
        }
        
        return result;
    }
}
