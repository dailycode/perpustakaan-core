/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.view;

/**
 *
 * @author krissadewo
 */
public class MainView extends javax.swing.JFrame {

    /**
     * Creates new form MainView
     */
    public MainView() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        menuMaster = new javax.swing.JMenu();
        menuItemBuku = new javax.swing.JMenuItem();
        menuItemAnggota = new javax.swing.JMenuItem();
        menuTransaksi = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        menuBar.setName(""); // NOI18N

        menuMaster.setText("Master");

        menuItemBuku.setText("Buku");
        menuItemBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemBukuActionPerformed(evt);
            }
        });
        menuMaster.add(menuItemBuku);

        menuItemAnggota.setText("Anggota");
        menuItemAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemAnggotaActionPerformed(evt);
            }
        });
        menuMaster.add(menuItemAnggota);

        menuBar.add(menuMaster);

        menuTransaksi.setText("Transaksi");
        menuBar.add(menuTransaksi);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 281, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(800, 600));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menuItemBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemBukuActionPerformed
        BukuView bukuView = new BukuView("Data Buku");
        this.add(bukuView);
        bukuView.show(); 
    }//GEN-LAST:event_menuItemBukuActionPerformed

    private void menuItemAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemAnggotaActionPerformed
       
    }//GEN-LAST:event_menuItemAnggotaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainView().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem menuItemAnggota;
    private javax.swing.JMenuItem menuItemBuku;
    private javax.swing.JMenu menuMaster;
    private javax.swing.JMenu menuTransaksi;
    // End of variables declaration//GEN-END:variables
}
