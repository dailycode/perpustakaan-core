/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import id.co.iconpln.perpustakaan.entity.Buku;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author krissadewo
 */
public class BukuDAOTest {

    private BukuDAO bukuDAO;
    private Buku buku;

    @Before
    public void init() {
        bukuDAO = new BukuDAOImpl();

        buku = new Buku();
        buku.setKode("B002");
        buku.setJudul("Pemrograman Java 11234");
        buku.setPengarang("H. Kusumo");
    }

    //@Test
    public void save() {
        Buku result = bukuDAO.save(buku);
        System.out.println(buku);
    }

    @Test
    public void find() {
        List<Buku> result = bukuDAO.find(new Buku());
        for (Buku buku : result) {
            System.out.println(buku);
        }
    }

}
