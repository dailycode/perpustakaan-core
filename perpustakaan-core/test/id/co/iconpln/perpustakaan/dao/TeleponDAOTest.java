/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.TeleponDAOImpl;
import id.co.iconpln.perpustakaan.entity.Anggota;
import id.co.iconpln.perpustakaan.entity.Buku;
import id.co.iconpln.perpustakaan.entity.Telepon;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author krissadewo
 */
public class TeleponDAOTest {

    private TeleponDAO teleponDAO;
    private Telepon telepon;

    @Before
    public void init() {
        teleponDAO = new TeleponDAOImpl();

        telepon = new Telepon();
        telepon.setNo("08112234");
        telepon.setAnggota(new Anggota(1));
    }

    @Test
    public void save() {
        Telepon result = teleponDAO.save(telepon);
        System.out.println(result);
    }


}
