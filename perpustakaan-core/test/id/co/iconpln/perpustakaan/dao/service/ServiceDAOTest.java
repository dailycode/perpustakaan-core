/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.service;

import id.co.iconpln.perpustakaan.dao.*;
import id.co.iconpln.perpustakaan.dao.impl.AnggotaDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.TeleponDAOImpl;
import id.co.iconpln.perpustakaan.entity.Anggota;
import id.co.iconpln.perpustakaan.entity.Buku;
import id.co.iconpln.perpustakaan.entity.Telepon;
import id.co.iconpln.perpustakaan.service.ServiceDAO;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author krissadewo
 */
public class ServiceDAOTest {

    private ServiceDAO serviceDAO;
    private Anggota anggota;
    private Telepon telepon;

    @Before
    public void init() {
        serviceDAO = new ServiceDAO();
    }

    @Test
    public void save() {
        Set<Telepon> telepons = new HashSet<>();
        Telepon telepon1 = new Telepon();
        telepon1.setNo("081123800234");
        telepons.add(telepon1);

        Telepon telepon2 = new Telepon();
        telepon2.setNo("085729090456");
        telepons.add(telepon2);

        anggota = new Anggota();
        anggota.setNo("1223");
        anggota.setNama("kris sadewo");
        anggota.setTelepons(telepons);

        serviceDAO.save(anggota);
    }

}
